'use strict';

const { ValidatorError, messages } = require('../validation')
  , utils = require('../queries/utils');

class Field {

  constructor(name, schema) {
    schema = schema || {};
    this.name = name;
    this.schema = schema;
    this.defaultValue = undefined;
    this.validators = [];
    this.setters = [];
    this.getters = [];
    this._currentValue = undefined;
    this.type = schema.type;

    // Common validators

    if (this.schema.required) {
      this.addValidator(function (value) {
        if (value === null || value === undefined) {
          return new ValidatorError({
            path: name,
            message: messages.general.required,
            kind: 'required',
            value: value
          });
        }
      });
    }

    if (schema.default) {
      this.defaultValue = schema.default;
    }
  }

  /**
   *
   * @param fn - function(value) {}
   * @returns {Field}
   */
  addSetter(fn) {
    if (typeof fn !== 'function') {
      throw new TypeError('A setter must be a function.');
    }
    this.setters.push(fn);
    return this;
  }

  /**
   *
   * @param fn - function(value) {}
   * @returns {Field}
   */
  addGetter(fn) {
    if (typeof fn !== 'function') {
      throw new TypeError('A getter must be a function.');
    }
    this.getters.push(fn);
    return this;
  }

  /**
   *
   * @param fn - function(value) {}
   * @returns {Field}
   */
  addValidator(fn) {
    if (typeof fn !== 'function') {
      throw new TypeError('A validator must be a function.');
    }
    this.validators.push(fn);
    return this;
  }

  getDefaultValue() {
    if (typeof this.defaultValue === 'function') {
      return this.defaultValue();
    }
    return this.defaultValue;
  }

  toQueryName() {
    return utils.normalizeName(this.name);
  }

  toCreateQuery() {
    return `${utils.normalizeName(this.name)} ${this.type} ${this.defaultValue === undefined ? '' : this.defaultValue}`;
  }

  _defineProperty(obj) {
    Object.defineProperty(obj, this.name, {
      get: () => {
        let value = this._currentValue;
        this.getters.forEach(getter => {
          value = getter(value);
        });
        return value;
      },
      set: (value) => {
        this.setters.forEach(setter => {
          value = setter(value);
        });
        this._currentValue = value;
        return value;
      }
    });
  }

}

module.exports = Field;
