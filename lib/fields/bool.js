'use strict';

const Field = require('./field');

class BooleanField extends Field {

  constructor(name, schema) {
    super(name, schema);

    this.addSetter(function (value) {
      if (typeof value !== 'number') {
        return 0;
      }
      return value > 0 ? 1 : 0;
    });

    this.addGetter(function (value) {
      return value === 1;
    });

  }

}

module.exports = BooleanField;
