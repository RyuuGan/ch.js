'use strict';

const StringField = require('./string');
const { ValidatorError, messages } = require('../validation');

class FixedStringField extends StringField {

  constructor(name, schema) {
    super(name, schema);

    if (schema.maxSize === undefined) {
      throw new Error('FixedStringField must contain maxSize');
    }

    this.maxSize = schema.maxSize;

    this.addValidator((value) => {
      if (value === undefined) {
        return;
      }

      let buffer = Buffer.from(value);

      if (buffer.length > this.maxSize) {
        return new ValidatorError({
          path: name,
          message: messages.String.maxFixedLength,
          maxlength: this.maxSize,
          kind: 'maxFixedString',
          value: value
        });
      }
    });

  }

}

module.exports = FixedStringField;
