'use strict';

const Field = require('./field');
const { ValidatorError, messages } = require('../validation');

class StringField extends Field {

  constructor(name, schema) {
    super(name, schema);

    if (schema.minlength) {
      this.addValidator(function (value) {
        if (value === undefined) {
          return;
        }
        if (value.length < schema.minlength) {
          return new ValidatorError({
            path: name,
            message: messages.String.minlength,
            minlength: schema.minlength,
            kind: 'minlength',
            value: value
          });
        }
      });
    }

    if (schema.maxlength) {
      this.addValidator(function (value) {
        if (value === undefined) {
          return;
        }
        if (value.length < schema.minlength) {
          return new ValidatorError({
            path: name,
            message: messages.String.maxlength,
            maxlength: schema.maxlength,
            kind: 'maxlength',
            value: value
          });
        }
      });
    }

    if (schema.match) {
      this.addValidator(function (value) {
        let regExp = schema.match;

        if (!regExp instanceof RegExp) {
          return;
        }

        if (value !== null && value !== '' && !regExp.test(value)) {
          return new ValidatorError({
            path: name,
            message: messages.String.match,
            kind: 'match',
            value: value
          });
        }

      });
    }

    if (schema.enum) {
      this.addValidator(function (value) {
        if (value === undefined) return;
        let idx = schema.enum.indexOf(value);

        if (idx === -1) {
          return new ValidatorError({
            path: name,
            message: messages.String.enum,
            kind: 'enum',
            value: value
          });
        }

      });
    }

  }

}

module.exports = StringField;
