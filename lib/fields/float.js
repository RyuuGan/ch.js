'use strict';

const Field = require('./field');
const { ValidatorError, messages } = require('../validation');

class FloatField extends Field {

  constructor(name, schema) {
    super(name, schema);
    this.type = schema.type || 'Float64';

    if (schema.min) {
      this.addValidator(function (value) {
        if (value === undefined || value === null) {
          return;
        }
        if (value < schema.min) {
          return new ValidatorError({
            path: name,
            message: messages.Number.min,
            kind: 'min',
            value: value
          });
        }
      });
    }

    if (schema.max) {
      this.addValidator(function (value) {
        if (value === undefined || value === null) {
          return;
        }
        if (value > schema.max) {
          return new ValidatorError({
            path: name,
            message: messages.Number.max,
            kind: 'max',
            value: value
          });
        }
      });
    }

  }

}

module.exports = FloatField;
