'use strict';

const Field = require('./field');
const { ValidatorError, messages } = require('../validation');

class DateField extends Field {

  constructor(name, schema) {
    super(name, schema);

    if (schema.min) {
      this.addValidator(function (value) {
        if (value === undefined) {
          return;
        }
        if (value.getTime() < schema.min.getTime()) {
          return new ValidatorError({
            path: name,
            message: messages.Date.min,
            kind: 'min',
            value: value
          });
        }
      });
    }

    if (schema.max) {
      this.addValidator(function (value) {
        if (value === undefined) {
          return;
        }
        if (value.getTime() > schema.max.getTime()) {
          return new ValidatorError({
            path: name,
            message: messages.Date.max,
            kind: 'max',
            value: value
          });
        }
      });
    }

  }

}

module.exports = DateField;
