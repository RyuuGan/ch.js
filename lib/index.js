'use strict';

const Model = require('./model')
  , Schema = require('./schema')
  , ClickHouse = require('./clickhouse');

class ChJS {

  /**
   *
   * @param {Object} [options]
   */
  constructor(options) {
    this.models = {};
    this.modelSchemas = {};
    this.options = {
      url: 'http://localhost',
      port: 8123,
      db: 'public',
      debug: false
    };
    this.options = Object.assign(this.options, options || {});

    this._clickhouse = new ClickHouse(this.options);


    this.Schema = Schema;
  }

  /**
   * @param {String} name
   * @param {Object|Schema} schema
   * @returns {Model}
   */
  model(name, schema) {
    if (!(schema instanceof Schema)) {
      schema = new Schema(schema);
    }
    let model = new Model(name, schema);
    this.models[name] = model;
    this.modelSchemas[name] = schema;

    return model;
  }

}

module.exports = new ChJS();
