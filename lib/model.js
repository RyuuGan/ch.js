'use strict';

const SelectQuery = require('./queries/select')
  , { CreateQuery } = require('./queries/create')
  , utils = require('./queries/utils');

class Model {

  constructor(name, schema) {
    this.schema = schema;
    this.name = name;
    this.schema.fields.forEach(f => f._defineProperty(this));
    this.format = 'TabSeparatedWithNamesAndTypes';
  }

  select(...fields) {

    let select = new SelectQuery()
      .from(utils.normalizeName(this.name))
      .format(this.format);

    let fieldsStr = fields.map(f => {
      if (typeof f === 'string') {
        return f;
      } else {
        return f.toQueryName();
      }
    });

    if (fieldsStr.length) {
      select.select(fieldsStr.join(', '));
    }

    return select;
  }

  /**
   * @returns {CreateTableQuery}
   */
  create() {
    const ChJS = require('./index');
    let create = CreateQuery
      .table()
      .name(this.name)
      .db(ChJS.options.db)
      .ifNotExists();

    let fields = this.schema.fields.map(f => f.toCreateQuery());
    create.fields(fields);

    return create;
  }

}

module.exports = Model;

