'use strict';

module.exports = exports = {};

exports.isObject = function (data) {
  return typeof data === 'object' && !Array.isArray(data);
};
