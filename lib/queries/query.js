'use strict';

class Query {

  exec() {
    const ChJs = require('../index');

    return new Promise((resolve, reject) => {
      let result = [];
      let rs = ChJs._clickhouse.query({
        body: this.toQuery()
      });

      rs.on('error', reject);
      rs.on('end', function () {
        resolve(result);
      });

      rs.on('data', (obj) => {
        result.push(obj)
      });
    });
  }

  toQuery() {
    return '';
  }

}

module.exports = Query;
