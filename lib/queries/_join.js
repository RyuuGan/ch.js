'use strict';

const Utils = require('./utils');

class JoinClause {

  constructor() {
    this.isGlobal = false;
    this.isOuter = false;
    this._anyAll = 'ANY';
    this._innerLeft = 'INNER';
    this._join = '';
    this._using = [];
  }

  any() {
    this._anyAll = 'ANY';
    return this;
  }

  all() {
    this._anyAll = 'ALL';
    return this;
  }

  inner() {
    this._innerLeft = 'INNER';
    return this;
  }

  left() {
    this._innerLeft = 'LEFT';
    return this;
  }

  using(field) {
    if (field) {
      this._using.push(Utils.normalizeName(field));
    }
    return this;
  }

  join(expr) {
    this._join = expr;
    return this;
  }

  toQuery() {
    let result = '';
    if (!this._join) {
      throw new Error('Join query or table must be set via `join` method');
    }
    if (!this._using.length) {
      throw new Error('Using fields must be set via `using` method');
    }

    if (this.isGlobal) {
      result += 'GLOBAL ';
    }

    result += this._anyAll + ' ';
    result += this._innerLeft + ' ';
    result += this.isOuter ? 'OUTER ' : '';
    result += `JOIN ${this._join} USING ${this._using.join(', ')}`;

    return Utils.replaceWhitespaces(result);
  }

}

module.exports.JoinClause = JoinClause;
