'use strict';

class Utils {

  static normalizeName(name) {
    name = name.trim();
    return name ? `\`${name}\``: '';
  }

  /**
   * Replaces multiple whitespaces from given string
   * @param str
   * @returns {string}
   */
  static replaceWhitespaces(str) {
    return str.replace(/ {2,}/g, ' ');
  }
}

module.exports = Utils;
