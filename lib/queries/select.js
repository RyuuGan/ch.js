'use strict';

const Utils = require('./utils')
  , Query = require('./query')
  , { JoinClause } = require('./_join');

class SelectQuery extends Query {
  constructor() {
    super();
    this._db = '';
    this._expr = '*';
    this._from = '';
    this._withTotals = false;
    this._join = '';
    this._orderBy = [];
    this._limit = '';
    this._union = '';
    this._format = '';

    ['where', 'prewhere', 'groupBy', 'having', 'arrayJoin'].forEach(op => {
      this[`_${op}`] = [];

      this[op] = function (expr) {
        if (expr) {
          this[`_${op}`].push(expr);
        }
        return this;
      };
    });

    this.defaultWhereOperator = 'AND';
    this.defaultHavingOperator = 'AND';
  }

  select(expression) {
    this._expr = expression;
    return this;
  }

  from(from) {
    this._from = from;
    return this;
  }

  /**
   * The SAMPLE clause allows for approximated query processing
   * @param {number} sample
   *
   * @see https://clickhouse.yandex/docs/en/query_language/queries.html#sample-clause
   */
  sample(sample) {
    this._sample = sample;
    return this;
  }

  withTotals() {
    this._withTotals = true;
    return this;
  }

  orderBy(field, sort = 'ASC', collate) {
    let result = '';
    if (field) {
      result += `${Utils.normalizeName(field)} ${sort}`
    }

    if (field && collate) {
      result += ` COLLATE '${collate}'`
    }
    if (result) {
      this._orderBy.push(result)
    }
    return this;
  }

  join(clause) {
    if (clause instanceof JoinClause) {
      this._join = ' ' + clause.toQuery();
    } else {
      this._join = ' ' + clause.toString();
    }
    return this;
  }

  limit(limit, skip) {
    if (limit) {
      this._limit = '';
      if (skip) {
        this._limit += `${skip}, `;
      }
      this._limit += limit.toString();

    }
    return this;
  }

  union(query) {
    if (query instanceof SelectQuery) {
      this._union = query.toQuery();
    } else {
      this._union = query;
    }
    return this;
  }

  format(format) {
    this._format = format;
    return this;
  }

  toQuery() {
    let result;
    if (!this._expr) {
      throw new Error('Expression must be set via `select` method');
    }

    result = `SELECT ${this._expr}`;

    if (this._from) {
      result += ` FROM ${this._from}`;
    }

    if (this._sample) {
      result += ` SAMPLE ${this._sample}`;
    }
    result += this._join ? this._join : '';

    result += this.mkArrayParams('arrayJoin', 'ARRAY JOIN');
    result += this.mkArrayParams('where', 'WHERE', this.defaultWhereOperator);
    result += this.mkArrayParams('prewhere', 'PREWHERE', this.defaultWhereOperator);

    if (this._groupBy.length) {
      result += this.mkArrayParams('groupBy', 'GROUP BY');

      if (this._withTotals) {
        result += ' WITH TOTALS';
      }

      result += this.mkArrayParams('having', 'HAVING', this.defaultHavingOperator);
    }

    if (this._orderBy.length) {
      result += ` ORDER BY ${this._orderBy.join(', ')}`
    }

    if (this._limit) {
      result += ` LIMIT ${this._limit}`;
    }

    if (this._union) {
      result += ` UNION ALL ${this._union}`;
    }

    if (this._format) {
      result += ` FORMAT ${this._format}`
    }

    return Utils.replaceWhitespaces(result);
  }

  // Helper functions

  mkArrayParams(arrayName, clause, operator = ', ') {
    let array = this[`_${arrayName}`];
    if (array.length) {
      if (operator !== ', ') {
        operator = ` ${operator} `;
      }
      return ` ${clause} ` + this[`_${arrayName}`].join(operator);
    }
    return '';
  }
}

module.exports.SelectQuery = SelectQuery;
