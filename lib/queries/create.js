'use strict';

const Utils = require('./utils')
  , Query = require('./query');

class CreateQuery {

  /**
   * @returns {CreateDatabaseQuery}
   */
  static db() {
    return new CreateDatabaseQuery();
  }

  /**
   * @returns {CreateTableQuery}
   */
  static table() {
    return new CreateTableQuery();
  }

}

class CreateDatabaseQuery extends Query {
  constructor() {
    super();
    this._db = '';
    this._ifNotExists = '';
  }

  ifNotExists() {
    this._ifNotExists = 'IF NOT EXISTS';
    return this;
  }

  name(name) {
    this._db = Utils.normalizeName(name);
    return this;
  }

  toQuery() {
    let result;
    if (!this._db) {
      throw new Error('DB name must be set via `name` method');
    }

    result = `CREATE DATABASE ${this._ifNotExists} ${this._db}`;

    return Utils.replaceWhitespaces(result);
  }
}

class CreateTableQuery extends Query {
  constructor() {
    super();
    this._db = '';
    this._table = '';
    this._ifNotExists = '';
    this._temporary = '';
    this._fields = '';
    this._engine = '';
  }

  db(name) {
    this._db = Utils.normalizeName(name);
    return this;
  }

  name(name) {
    this._table = Utils.normalizeName(name);
    return this;
  }

  cluster(name) {
    this._cluster = Utils.normalizeName(name);
    return this;
  }

  ifNotExists() {
    this._ifNotExists = 'IF NOT EXISTS';
    return this;
  }

  temporary() {
    this._temporary = 'TEMPORARY';
    return this;
  }

  fields(fields) {
    this._fields = fields;
    return this;
  }

  engine(engine) {
    this._engine = engine;
    return this;
  }

  toQuery() {
    let result;
    if (!this._table) {
      throw new Error('Table name must be set via `name` method');
    }

    if (!this._fields) {
      throw new Error('Table fields must be set via `fields` method');
    }

    if (!this._engine) {
      throw new Error('Table engine must be set via `engine` method');
    }

    let tableName = this._db ? `${this._db}.${this._table}` : `${this._table}`;
    let onCluster = this._cluster ? `ON CLUSTER ${this._cluster}` : '';
    let engine = this._engine ? `ENGINE = ${this._engine}` : '';
    result = `CREATE ${this._temporary} TABLE ${this._ifNotExists} ${tableName} ${onCluster} ( ${this._fields} ) ${engine}`;

    return Utils.replaceWhitespaces(result).trim();
  }
}

module.exports = {
  CreateQuery,
  CreateDatabaseQuery,
  CreateTableQuery
};

