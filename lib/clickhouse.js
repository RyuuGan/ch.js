'use strict';

const request = require('request-promise')
  , stream = require('stream')
  , { TypeCast } = require('./schema/cast')
  , debug = require('debug')('ch.js:debug');

const lineSep = '\n';

class ClickHouse {
  constructor(_opts) {
    this.opts = _opts;

    if (this.opts.typeCast instanceof TypeCast) {
      this.typeCast = this.opts.typeCast;
    } else {
      this.typeCast = new TypeCast();
    }
  }

  getHost() {
    return this.opts.url + ':' + this.opts.port;
  }

  /**
   * Parse data
   * @param {Buffer} data
   * @returns {Array}
   */
  _parseData(data) {
    let self = this,
      rows = data.toString('utf8'),
      columnList = [],
      typeList = [];

    if (!rows) return [];

    rows = rows.split('\n');
    columnList = rows[0].split('\t');
    typeList = rows[1].split('\t');

    // Удаляем строки с заголовками и типами столбцов И завершающую строку
    rows = rows.slice(2, rows.length - 1);

    columnList = columnList.reduce(
      function (arr, column, i) {
        arr.push({
          name: column,
          type: typeList[i]
        });

        return arr;
      },
      []
    );

    return rows.map(function (row) {
      let columns = row.split('\t');

      return columnList.reduce(
        function (obj, column, i) {
          obj[column.name] = self.typeCast.cast(column.type, columns[i]);
          return obj;
        },
        {}
      );
    });
  }

  /**
   * Exec query
   * @param {String|Object} _opts
   * @param {Function} cb
   * @returns {Stream|undefined}
   */
  query(_opts, cb) {
    let self = this
      , opts = {
      body: ''
    };

    if (!_opts) {
      let err = new Error('First params should not empty');
      if (cb) {
        return cb(err);
      } else {
        throw err;
      }
    }

    if (typeof opts === 'string') {
      opts.body = _opts;
    } else {
      opts = Object.assign(opts, _opts);
    }

    let reqParams = {
      url: this.getHost(),
      body: opts.body,
      headers: {
        'Content-Type': 'text/plain'
      }
    };

    if (cb) {
      return request.post(
        reqParams,
        function (error, response, body) {
          if (error) return cb(error);

          if (response.statusCode === 200 && response.statusMessage === 'OK')
            return cb(null, self._parseData(body));

          // Если попали сюда - значит что-то пошло не так
          cb(response.body || response.statusMessage);
        }
      );
    } else {
      let rs = new stream.Readable();
      rs._read = function (chunk) {
        debug('rs _read chunk %s', chunk);
      };

      let queryStream = request.post(reqParams);
      queryStream.columnsName = null;

      let responseStatus = 200;
      let str = '';

      queryStream
        .on('response', function (response) {
          responseStatus = response.statusCode;
        })
        .on('error', function (err) {
          rs.emit('error', err);
        })
        .on('data', function (data) {
          str = str + data.toString('utf8');

          if (responseStatus !== 200) return rs.emit('error', str);

          let lineSepIndex = str.lastIndexOf(lineSep);
          if (lineSepIndex === -1) return true;

          let rows = str.substr(0, lineSepIndex).split(lineSep);
          str = str.substr(lineSepIndex + 1);

          if (!queryStream.columnList) {
            let columnList = rows[0].split('\t');
            let typeList = rows[1].split('\t');

            // Удаляем строки с заголовками и типами столбцов И завершающую строку
            //console.log('stream', rows, rows.length, rows.slice(2, rows.length - 1));
            rows = rows.slice(2, rows.length);

            queryStream.columnList = columnList.reduce(
              function (arr, column, i) {
                arr.push({
                  name: column,
                  type: typeList[i]
                });

                return arr;
              },
              []
            );

            debug('Columns', queryStream.columnList);
          }

          debug('Raw data', data.toString('utf8'));

          for (let i = 0; i < rows.length; i++) {
            let columns = rows[i].split('\t');
            rs.emit(
              'data',
              queryStream.columnList.reduce(
                (o, c, i) => {
                  o[c.name] = self.typeCast.cast(c.type, columns[i]);
                  return o;
                },
                {}
              )
            );
          }
        })
        .on('end', function () {
          rs.emit('end');
        });

      return rs;
    }
  }
}

module.exports = ClickHouse;
