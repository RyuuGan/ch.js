/*!
 * Module dependencies.
 */

const { CHError, messages } = require('./index');

/**
 * Schema validator error
 *
 * @param {Object} properties
 * @inherits CHError
 * @api private
 */

function ValidatorError(properties) {
  let msg = properties.message;
  if (!msg) {
    msg = messages.general.default;
  }

  let message = this.formatMessage(msg, properties);
  CHError.call(this, message);
  this.name = 'ValidatorError';
  if (Error.captureStackTrace) {
    Error.captureStackTrace(this);
  } else {
    this.stack = new Error().stack;
  }
  this.properties = properties;
  this.kind = properties.kind;
  this.path = properties.path;
  this.value = properties.value;
  this.reason = properties.reason;
}

/*!
 * Inherits from CHError
 */

ValidatorError.prototype = Object.create(CHError.prototype);
ValidatorError.prototype.constructor = CHError;

/*!
 * The object used to define this validator. Not enumerable to hide
 * it from `require('util').inspect()` output re: gh-3925
 */

Object.defineProperty(ValidatorError.prototype, 'properties', {
  enumerable: false,
  writable: true,
  value: null
});

/*!
 * Formats error messages
 */

ValidatorError.prototype.formatMessage = function(msg, properties) {
  let propertyNames = Object.keys(properties);
  for (let i = 0; i < propertyNames.length; ++i) {
    let propertyName = propertyNames[i];
    if (propertyName === 'message') {
      continue;
    }
    msg = msg.replace('{' + propertyName.toUpperCase() + '}', properties[propertyName]);
  }
  return msg;
};

/*!
 * toString helper
 */
ValidatorError.prototype.toString = function() {
  return this.message;
};

/*!
 * exports
 */

module.exports = ValidatorError;
