'use strict';

/**
 * CHError constructor
 *
 * @param {String} msg Error message
 * @inherits Error https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Error
 */
function CHError(msg) {
  Error.call(this);
  if (Error.captureStackTrace) {
    Error.captureStackTrace(this);
  } else {
    this.stack = new Error().stack;
  }
  this.message = msg;
  this.name = 'CHError';
}

/*!
 * Inherits from Error.
 */
CHError.prototype = Object.create(Error.prototype);
CHError.prototype.constructor = Error;

/*!
 * Module exports.
 */

module.exports = exports = CHError;

module.exports = exports = { CHError };

exports.messages = require('./messages');
exports.ValidationError = require('./validation');
exports.ValidatorError = require('./validator');
