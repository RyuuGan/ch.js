'use strict';

const { CHError } = require('./index');

class ValidationError extends CHError {
  constructor(instance) {
    super(message);
    this.message = message;
    this.field = field;
  }

  toString() {
    return `ValidationError()`;
  }
}
