'use strict';

const utils = require('../utils')
  , Types = require('./cast').Types
  , err = require('debug')('ch.js:err');

const {
  BooleanField,
  DateField,
  IntField,
  FloatField,
  StringField,
  FixedStringField
} = require('../fields');

class Schema {
  constructor(schema) {
    schema = schema || {};

    this._schema = schema;

    this.fields = Object.keys(this._schema).map(key => {
      let _value = this._schema[key];

      let type = this._getType(_value);
      let _fieldSchema = utils.isObject(_value) ? _value : { type };

      let field;

      switch (type) {
        case String:
        case Types.String:
          _fieldSchema.type = Types.String;
          if (_value.maxSize && _value.maxSize > 0) {
            _fieldSchema.maxSize = _value.maxSize;
            field = new FixedStringField(key, _fieldSchema);
          } else {
            field = new StringField(key, _fieldSchema);
          }
          break;
        case Boolean:
        case Types.Boolean:
          _fieldSchema.type = Types.Boolean;
          field = new BooleanField(key, _fieldSchema);
          break;
        case Date:
        case Types.Date:
          _fieldSchema.type = Types.Date;
          field = new DateField(key, _fieldSchema);
          break;
        case Types.DateTime:
          _fieldSchema.type = Types.DateTime;
          field = new DateField(key, _fieldSchema);
          break;

        case Number:
        case Types.Float32:
          _fieldSchema.type = Types.Float32;
          field = new FloatField(key, _fieldSchema);
          break;
        case Types.Float64:
          _fieldSchema.type = Types.Float64;
          field = new FloatField(key, _fieldSchema);
          break;
        case Types.Int8:
          _fieldSchema.type = Types.Int8;
          field = new IntField(key, _fieldSchema);
          break;
        case Types.Int16:
          _fieldSchema.type = Types.Int16;
          field = new IntField(key, _fieldSchema);
          break;
        case Types.Int32:
          _fieldSchema.type = Types.Int32;
          field = new IntField(key, _fieldSchema);
          break;
        case Types.Int64:
          _fieldSchema.type = Types.Int64;
          field = new IntField(key, _fieldSchema);
          break;
        case Types.UInt8:
          _fieldSchema.type = Types.UInt8;
          field = new IntField(key, _fieldSchema);
          break;
        case Types.UInt16:
          _fieldSchema.type = Types.UInt16;
          field = new IntField(key, _fieldSchema);
          break;
        case Types.UInt32:
          _fieldSchema.type = Types.UInt32;
          field = new IntField(key, _fieldSchema);
          break;
        case Types.UInt64:
          _fieldSchema.type = Types.UInt64;
          field = new IntField(key, _fieldSchema);
          break;
        default:
          err('Unknown type of field %s. Using Int field by default', JSON.stringify(_value));
          field = new IntField(key, _fieldSchema);
          break;
      }

      return field;
    });

    this.fields.forEach(f => this.fields[f.name] = f);
  }

  _getType(value) {

    if (utils.isObject(value)) {
      return value.type;
    }

    return value;
  }

}

module.exports = Schema;
