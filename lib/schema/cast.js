'use strict';

const intTypeList = ['UInt8', 'UInt16', 'UInt32', 'UInt64', 'Int8', 'Int16', 'Int32', 'Int64'];
const floatTypeList = ['Float32', 'Float64'];

class TypeCast {

  constructor() {
    this.castMap = intTypeList.reduce(
      (obj, type) => {
        obj[type] = value => parseInt(value, 10);
        return obj;
      },
      {
        'Date': value => new Date(value),
        'DateTime': value => new Date(value),
        'String': value => value
      }
    );
    floatTypeList.forEach(t => {
      this.castMap[t] = value => parseFloat(value);
    });
  }

  cast(type, value) {
    return this.castMap[type] ? this.castMap[type](value) : value;
  }

  install(type, fn) {
    this.castMap[type] = fn;
  }

}

module.exports.TypeCast = TypeCast;
module.exports.intTypeList = intTypeList;
module.exports.floatTypeList = floatTypeList;
module.exports.Types = {
  String: 'String',
  Date: 'Date',
  DateTime: 'DateTime',
  Boolean: 'Boolean',
  UInt8: 'UInt8',
  UInt16: 'UInt16',
  UInt32: 'UInt32',
  UInt64: 'UInt64',
  Int8: 'Int8',
  Int16: 'Int16',
  Int32: 'Int32',
  Int64: 'Int64',
  Float32: 'Float32',
  Float64: 'Float64'
};
