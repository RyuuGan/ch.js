'use strict';

const ChJs = require('../../lib/index')
  , Schema = ChJs.Schema;

const Country = new Schema({
  code: String,
  capital: {
    type: String,
    ref: 'CountryCity'
  }
});

/**
 * @type {Model}
 */
module.exports = ChJs.model('Country', Country);


