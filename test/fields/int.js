'use strict';

let { intTypeList } = require('../../lib/schema/cast')
  , IntField = require('../../lib/fields/int')
  , ValidatorError = require('../../lib/validation/validator');

intTypeList.forEach(function (fieldType) {

  describe(`${fieldType}`, function () {

    it('should create field type with empty schema', function () {

      let field = new IntField('myName', {
        type: fieldType
      });

      assert.strictEqual(fieldType, field.type, 'Field type must be same as requested');
      assert.strictEqual(field.name, 'myName', 'Field name must be myName');
      assert.strictEqual(field.validators.length, 0, 'No validators must be set');
      assert.strictEqual(field.setters.length, 0, 'No setters must be set');
      assert.strictEqual(field.getters.length, 0, 'No getters must be set');

    });

    it('should create field type with default (value based) schema', function () {

      let field = new IntField('myName', {
        default: 2
      });

      assert.strictEqual(field.type, 'Int64', 'Field type must be Int64 by default');
      assert.strictEqual(typeof field.defaultValue, 'number', 'Default value must be a number');
      assert.strictEqual(field.getDefaultValue(), 2, 'Default value must return 2');
      assert.strictEqual(field.validators.length, 0, 'No validators must be set');
      assert.strictEqual(field.setters.length, 0, 'No setters must be set');
      assert.strictEqual(field.getters.length, 0, 'No getters must be set');

    });

    it('should create field type with default (function based) schema', function () {

      let field = new IntField('myName', {
        default: function () {
          return 2;
        }
      });

      assert.strictEqual(typeof field.defaultValue, 'function', 'Default value must be a function');
      assert.strictEqual(field.getDefaultValue(), 2, 'Default value must return 2');
      assert.strictEqual(field.validators.length, 0, 'No validators must be set');
      assert.strictEqual(field.setters.length, 0, 'No setters must be set');
      assert.strictEqual(field.getters.length, 0, 'No getters must be set');

    });

    it('should create field type with default required schema', function () {

      let field = new IntField('myName', {
        required: true
      });

      assert.strictEqual(field.validators.length, 1, 'Required validator must be set');
      assert.strictEqual(field.setters.length, 0, 'No setters must be set');
      assert.strictEqual(field.getters.length, 0, 'No getters must be set');

    });

    it('should create field type with default (min, required) schema', function () {

      let field = new IntField('myName', {
        required: true,
        min: 2
      });

      assert.strictEqual(field.validators.length, 2, '2 validators must be set');
      assert.strictEqual(field.setters.length, 0, 'No setters must be set');
      assert.strictEqual(field.getters.length, 0, 'No getters must be set');

    });

    it('should create field type with default (min, max, required) schema', function () {

      let field = new IntField('myName', {
        required: true,
        min: 2,
        max: 20
      });

      assert.strictEqual(field.validators.length, 3, '3 validators must be set');
      assert.strictEqual(field.setters.length, 0, 'No setters must be set');
      assert.strictEqual(field.getters.length, 0, 'No getters must be set');

    });

    it('should create properly validate (min, max, required) schema', function () {

      let field = new IntField('myName', {
        required: true,
        min: 2,
        max: 20
      });

      for (let i = 0; i < field.validators.length; i++) {
        assert.isFunction(field.validators[i], 'Validator must be a function');
      }

      // Required
      let error = field.validators[0](null);
      assert.instanceOf(error, ValidatorError, 'Must be an instance of ValidatorError');
      assert.strictEqual(error.path, 'myName', 'Path must be myName');
      assert.strictEqual(error.kind, 'required', 'Kind must be required');

      error = field.validators[0](undefined);
      assert.instanceOf(error, ValidatorError, 'Must be an instance of ValidatorError');
      assert.strictEqual(error.path, 'myName', 'Path must be myName');
      assert.strictEqual(error.kind, 'required', 'Kind must be required');

      error = field.validators[0](1);
      assert.isUndefined(error, 'Error must not be defined');

      // Min
      error = field.validators[1](null);
      assert.isUndefined(error, 'Error must not be defined');

      error = field.validators[1](undefined);
      assert.isUndefined(error, 'Error must not be defined');

      error = field.validators[1](2);
      assert.isUndefined(error, 'Error must not be defined');

      error = field.validators[1](3);
      assert.isUndefined(error, 'Error must not be defined');

      error = field.validators[1](1);
      assert.instanceOf(error, ValidatorError, 'Must be an instance of ValidatorError');
      assert.strictEqual(error.path, 'myName', 'Path must be myName');
      assert.strictEqual(error.kind, 'min', 'Kind must be min');

      // Max
      error = field.validators[2](null);
      assert.isUndefined(error, 'Error must not be defined');

      error = field.validators[2](undefined);
      assert.isUndefined(error, 'Error must not be defined');

      error = field.validators[2](20);
      assert.isUndefined(error, 'Error must not be defined');

      error = field.validators[2](2);
      assert.isUndefined(error, 'Error must not be defined');

      error = field.validators[2](21);
      assert.instanceOf(error, ValidatorError, 'Must be an instance of ValidatorError');
      assert.strictEqual(error.path, 'myName', 'Path must be myName');
      assert.strictEqual(error.kind, 'max', 'Kind must be min');
    });

  });

});
