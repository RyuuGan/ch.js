'use strict';

const Field = require('../../lib/fields/field')
  , ValidatorError = require('../../lib/validation/validator');

describe('Field', function () {

  it('should create field without schema', function () {
    let field = new Field('myField');
    assert.equal(field.name, 'myField', 'Field name should be set as expected');
  });

  it('should process default as value', function () {
    let field = new Field('myField', {
      default: 'SomeValue'
    });

    assert.strictEqual(typeof field.defaultValue, 'string', 'Default value must be a number');
    assert.strictEqual(field.getDefaultValue(), 'SomeValue', 'Default value must return 2');
  });

  it('should process default as function', function () {
    let field = new Field('myField', {
      default: () => 'SomeValue'
    });

    assert.strictEqual(typeof field.defaultValue, 'function', 'Default value must be a number');
    assert.strictEqual(field.getDefaultValue(), 'SomeValue', 'Default value must return 2');
  });

  it('should add default required validator from schema', function () {
    let field = new Field('myField', {
      required: true
    });

    assert.strictEqual(field.validators.length, 1, 'Required validators must be set');

    // Required
    let error = field.validators[0](null);
    assert.instanceOf(error, ValidatorError, 'Must be an instance of ValidatorError');
    assert.strictEqual(error.path, 'myField', 'Path must be myField');
    assert.strictEqual(error.kind, 'required', 'Kind must be required');

    error = field.validators[0](undefined);
    assert.instanceOf(error, ValidatorError, 'Must be an instance of ValidatorError');
    assert.strictEqual(error.path, 'myField', 'Path must be myField');
    assert.strictEqual(error.kind, 'required', 'Kind must be required');

    error = field.validators[0](1);
    assert.isUndefined(error, 'Error must not be defined');

    error = field.validators[0]('Another value');
    assert.isUndefined(error, 'Error must not be defined');
  });

  it('should add validators function', function () {
    let field = new Field('myField');

    field.addValidator(function (value) {
      if (typeof value !== 'string') {
        return new ValidatorError({
          path: field.name,
          message: 'Path `{PATH}` is not a string ({VALUE}).',
          kind: 'string',
          value: value
        })
      }
    });

    assert.strictEqual(field.validators.length, 1, '1 validator must be set');

    let error = field.validators[0](undefined);
    assert.instanceOf(error, ValidatorError, 'Must be an instance of ValidatorError');
    assert.strictEqual(error.path, 'myField', 'Path must be myField');
    assert.strictEqual(error.kind, 'string', 'Kind must be string');
  });

  it('should not add non function validators', function () {
    let field = new Field('myField');

    let fn = field.addValidator.bind(this, 'Some Non function Value');
    assert.throws(fn, TypeError, 'A validator must be a function.', 'Exception must be thrown');
  });

});
