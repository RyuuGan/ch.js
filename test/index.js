'use strict';

require('./_config');

describe('ch.js', () => {

  require('./queries');
  require('./cast');
  require('./fields');
  require('./model');

});
