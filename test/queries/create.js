'use strict';

const { CreateQuery } = require('../../lib/queries/create');

describe('create', function () {

  it('should properly output create DATABASE query', function () {
    let q = CreateQuery
      .db()
      .name('example')
      .toQuery();

    assert.equal(q, 'CREATE DATABASE `example`', 'Should create simple database');
  });

  it('should use IF NOT EXISTS properly', function () {
    let q = CreateQuery.db()
      .name('example')
      .ifNotExists()
      .toQuery();

    assert.equal(q, 'CREATE DATABASE IF NOT EXISTS `example`', 'Should create simple database');
  });

  it('should throw an exception if db name is not set', function () {

    let query = CreateQuery
      .db()
      .ifNotExists();

    assert.throws(query.toQuery.bind(query), 'DB name must be set via `name` method', 'Exception must be thrown');

  });

  it('should properly output create TABLE query', function () {
    let q = CreateQuery
      .table()
      .name('example')
      .fields('RegionID UInt32')
      .engine('Null')
      .toQuery();

    assert.equal(q, 'CREATE TABLE `example` ( RegionID UInt32 ) ENGINE = Null', 'Should create simple database');
  });

  it('should properly output create TEMPORARY TABLE query', function () {
    let q = CreateQuery
      .table()
      .name('example')
      .temporary()
      .fields('RegionID UInt32')
      .engine('Null')
      .toQuery();

    assert.equal(q, 'CREATE TEMPORARY TABLE `example` ( RegionID UInt32 ) ENGINE = Null', 'Should create simple database');
  });

  it('should properly output create TABLE query with engine', function () {
    let q = CreateQuery
      .table()
      .name('example')
      .engine('Memory')
      .fields('RegionID UInt32')
      .toQuery();

    assert.equal(q, 'CREATE TABLE `example` ( RegionID UInt32 ) ENGINE = Memory', 'Should create simple database');
  });

  it('should properly output create TABLE query with db', function () {
    let q = CreateQuery
      .table()
      .db('public')
      .name('example')
      .fields('RegionID UInt32')
      .engine('Null')
      .toQuery();

    assert.equal(q, 'CREATE TABLE `public`.`example` ( RegionID UInt32 ) ENGINE = Null', 'Should create simple database');
  });

  it('should properly output create TABLE query with cluster', function () {
    let q = CreateQuery
      .table()
      .name('example')
      .fields('RegionID UInt32')
      .cluster('example')
      .engine('Null')
      .toQuery();

    assert.equal(q, 'CREATE TABLE `example` ON CLUSTER `example` ( RegionID UInt32 ) ENGINE = Null', 'Should create simple database');
  });

  it('should throw an exception if table name is not set', function () {

    let query = CreateQuery
      .table()
      .fields('RegionID UInt32')
      .cluster('example')
      .engine('Null');

    assert.throws(query.toQuery.bind(query), 'Table name must be set via `name` method', 'Exception must be thrown');

  });

  it('should throw an exception if table fields are not set', function () {

    let query = CreateQuery
      .table()
      .name('example')
      .cluster('example')
      .engine('Null');

    assert.throws(query.toQuery.bind(query), 'Table fields must be set via `fields` method', 'Exception must be thrown');

  });

  it('should throw an exception if table engine is not set', function () {

    let query = CreateQuery
      .table()
      .name('example')
      .fields('RegionID UInt32')
      .cluster('example');

    assert.throws(query.toQuery.bind(query), 'Table engine must be set via `engine` method', 'Exception must be thrown');

  });

});
