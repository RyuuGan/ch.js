'use strict';

const { JoinClause } = require('../../lib/queries/_join');

describe('JOIN', function () {

  it('should throw an exception if join is not set', function () {

    let query = new JoinClause();

    assert.throws(query.toQuery.bind(query), 'Join query or table must be set via `join` method', 'Exception must be thrown');

  });

  it('should throw an exception if using fields are not set', function () {

    let query = new JoinClause().join('`y`');

    assert.throws(query.toQuery.bind(query), 'Using fields must be set via `using` method', 'Exception must be thrown');

  });

  it('should properly output join with any join expression', function () {
    let q = new JoinClause()
      .join('(SELECT x, y1, y2 FROM (SELECT x, y AS y1 FROM t1) ALL INNER JOIN (SELECT x, y AS y2 FROM t2) USING `x`)')
      .using('x')
      .toQuery();

    assert.equal(q, 'ANY INNER JOIN (SELECT x, y1, y2 FROM (SELECT x, y AS y1 FROM t1) ALL INNER JOIN (SELECT x, y AS y2 FROM t2) USING `x`) USING `x`', 'Should create join clause');
  });

  it('ANY INNER should be default for queries', function () {
    let q = new JoinClause()
      .join('`y`')
      .using('x')
      .toQuery();

    assert.equal(q, 'ANY INNER JOIN `y` USING `x`', 'Should create join clause');
  });

  it('ANY INNER may be changed (ALL LEFT)', function () {
    let q = new JoinClause()
      .all()
      .left()
      .join('`y`')
      .using('x')
      .toQuery();

    assert.equal(q, 'ALL LEFT JOIN `y` USING `x`', 'Should create join clause');
  });

  it('ANY INNER may be not changed', function () {
    let q = new JoinClause()
      .any()
      .inner()
      .join('`y`')
      .using('x')
      .toQuery();

    assert.equal(q, 'ANY INNER JOIN `y` USING `x`', 'Should create join clause');
  });

});
