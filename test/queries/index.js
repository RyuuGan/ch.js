'use strict';

describe('queries', function () {

  require('./create');
  require('./select');
  require('./join');

});
