'use strict';

const { SelectQuery } = require('../../lib/queries/select');
const { JoinClause } = require('../../lib/queries/_join');

describe('create', function () {

  it('should throw an exception if no expression specified', function () {
    let query = new SelectQuery().select();

    assert.throws(query.toQuery.bind(query), 'Expression must be set via `select` method', 'Exception must be thrown');
  });

  it('should create simple select', function () {
    let q = new SelectQuery()
      .select(1)
      .toQuery();

    assert.equal(q, 'SELECT 1', 'Should create simple queries');
  });

  it('should create simple select with no select method', function () {
    let q = new SelectQuery()
      .toQuery();

    assert.equal(q, 'SELECT *', 'Should create simple queries');
  });

  it('should create simple query with from', function () {
    let q = new SelectQuery()
      .from('`example`')
      .toQuery();

    assert.equal(q, 'SELECT * FROM `example`', 'Should create simple queries');
  });

  it('should create complex queries', function () {
    let q0 = new SelectQuery().select('*').from('`example`');
    let q1 = new SelectQuery().from(q0.toQuery());

    let q = q1.toQuery();

    assert.equal(q, 'SELECT * FROM SELECT * FROM `example`', 'It should combine queries');

  });

  it('should create sample queries', function () {
    let q = new SelectQuery()
      .from('`example`')
      .sample(.1)
      .toQuery();

    let q2 = new SelectQuery()
      .from('`example`')
      .sample(10000000)
      .toQuery();

    assert.equal(q, 'SELECT * FROM `example` SAMPLE 0.1', 'It should sample queries with decimals');
    assert.equal(q2, 'SELECT * FROM `example` SAMPLE 10000000', 'It should sample queries with big integers');
  });

  it('should create array join queries', function () {
    let q = new SelectQuery()
      .select('s, arr')
      .from('arrays_test')
      .arrayJoin('arr')
      .toQuery();

    assert.equal(q, 'SELECT s, arr FROM arrays_test ARRAY JOIN arr', 'It should array join');
  });

  it ('should create multiple array joins', function () {
    let q = new SelectQuery()
      .select('s, arr, a, num, mapped')
      .from('arrays_test')
      .arrayJoin('arr AS a')
      .arrayJoin('arrayEnumerate(arr) AS num')
      .arrayJoin('arrayMap(lambda(tuple(x), plus(x, 1)), arr) AS mapped')
      .toQuery();

    assert.equal(q, 'SELECT s, arr, a, num, mapped FROM arrays_test ARRAY JOIN arr AS a, arrayEnumerate(arr) AS num, arrayMap(lambda(tuple(x), plus(x, 1)), arr) AS mapped', 'It should multiple array joins');
  });

  it('should create array joins with nested structures', function () {
    let q = new SelectQuery()
      .select('s, n.x, n.y, nest.x, nest.y, num')
      .from('nested_test')
      .arrayJoin('nest AS n')
      .arrayJoin('arrayEnumerate(nest.x) AS num')
      .toQuery();

    assert.equal(q, 'SELECT s, n.x, n.y, nest.x, nest.y, num FROM nested_test ARRAY JOIN nest AS n, arrayEnumerate(nest.x) AS num', 'It should create queries with nest');
  });

  it('should create where clause (AND)', function () {
    let q = new SelectQuery()
      .from('`test`')
      .where('A = 30')
      .where('B = 40')
      .toQuery();

    assert.equal(q, 'SELECT * FROM `test` WHERE A = 30 AND B = 40', 'It should create queries with where');
  });

  it('should create where clause (OR)', function () {
    let query = new SelectQuery()
      .from('`test`')
      .where('A = 30')
      .where('B = 40');
    query.defaultWhereOperator = 'OR';
    let q = query.toQuery();

    assert.equal(q, 'SELECT * FROM `test` WHERE A = 30 OR B = 40', 'It should create queries with where');
  });

  it('should create prewhere clause (AND)', function () {
    let q = new SelectQuery()
      .from('`test`')
      .prewhere('A = 30')
      .prewhere('B = 40')
      .toQuery();

    assert.equal(q, 'SELECT * FROM `test` PREWHERE A = 30 AND B = 40', 'It should create queries with where');
  });

  it('should create prewhere clause (OR)', function () {
    let query = new SelectQuery()
      .from('`test`')
      .prewhere('A = 30')
      .prewhere('B = 40');
    query.defaultWhereOperator = 'OR';
    let q = query.toQuery();

    assert.equal(q, 'SELECT * FROM `test` PREWHERE A = 30 OR B = 40', 'It should create queries with where');
  });

  it('should create group by clause', function () {
    let q = new SelectQuery()
      .from('`test`')
      .groupBy('orderNumber')
      .groupBy('orderAmount')
      .toQuery();

    assert.equal(q, 'SELECT * FROM `test` GROUP BY orderNumber, orderAmount', 'It should create queries with group by');
  });

  it('should create group by clause with totals', function () {
    let q = new SelectQuery()
      .from('`test`')
      .groupBy('orderNumber')
      .withTotals()
      .toQuery();

    assert.equal(q, 'SELECT * FROM `test` GROUP BY orderNumber WITH TOTALS', 'It should create queries with group by');
  });

  it('should create having clause (AND)', function () {
    let q = new SelectQuery()
      .from('`test`')
      .groupBy('orderNumber')
      .having('A = 30')
      .having('B = 40')
      .toQuery();

    assert.equal(q, 'SELECT * FROM `test` GROUP BY orderNumber HAVING A = 30 AND B = 40', 'It should create queries with having');
  });

  it('should create having clause (OR)', function () {
    let query = new SelectQuery()
      .from('`test`')
      .groupBy('orderNumber')
      .having('A = 30')
      .having('B = 40');
    query.defaultHavingOperator = 'OR';
    let q = query.toQuery();

    assert.equal(q, 'SELECT * FROM `test` GROUP BY orderNumber HAVING A = 30 OR B = 40', 'It should create queries with having');
  });

  it('should not have having clause if orderBy is not supplied', function () {

    let q = new SelectQuery()
      .from('`test`')
      .having('A = 30')
      .having('B = 40')
      .toQuery();

    assert.equal(q, 'SELECT * FROM `test`', 'It should not create having clause');

  });

  it('should process JOIN queries', function () {
    let q = new SelectQuery()
      .select('x, y')
      .from('`x`')
      .join('ALL INNER JOIN `y` USING `x`')
      .toQuery();

    assert.equal(q, 'SELECT x, y FROM `x` ALL INNER JOIN `y` USING `x`', 'It should create join query');
  });

  it('should process JOIN queries with JOIN clause', function () {

    let join = new JoinClause()
      .any()
      .left()
      .join('`y`')
      .using('x');

    let q = new SelectQuery()
      .select('x, y')
      .from('`x`')
      .join(join)
      .toQuery();

    assert.equal(q, 'SELECT x, y FROM `x` ANY LEFT JOIN `y` USING `x`', 'It should create join query');

  });

  it('should process ORDER BY queries', function () {
    let q = new SelectQuery()
      .select('x, y')
      .from('`x`')
      .orderBy('x')
      .toQuery();

    assert.equal(q, 'SELECT x, y FROM `x` ORDER BY `x` ASC', 'It should process order by and set ASC sorting by default');
  });

  it('should process ORDER BY queries with 2 order clauses', function () {
    let q = new SelectQuery()
      .select('x, y')
      .from('`x`')
      .orderBy('x')
      .orderBy('y', 'DESC')
      .toQuery();

    assert.equal(q, 'SELECT x, y FROM `x` ORDER BY `x` ASC, `y` DESC', 'It should process order by and set ASC sorting by default');
  });

  it('should process ORDER BY queries with 2 order clauses and collate', function () {
    let q = new SelectQuery()
      .select('x, y')
      .from('`x`')
      .orderBy('x', 'DESC', 'ru')
      .orderBy('y', 'DESC')
      .toQuery();

    assert.equal(q, 'SELECT x, y FROM `x` ORDER BY `x` DESC COLLATE \'ru\', `y` DESC', 'It should process order by and set ASC sorting by default');
  });

  it('should process LIMIT queries', function () {
    let q = new SelectQuery()
      .select('x, y')
      .from('`x`')
      .limit(10)
      .toQuery();

    assert.equal(q, 'SELECT x, y FROM `x` LIMIT 10', 'It should process limit queries');
  });

  it('should process LIMIT queries with skip', function () {
    let q = new SelectQuery()
      .select('x, y')
      .from('`x`')
      .limit(10, 5)
      .toQuery();

    assert.equal(q, 'SELECT x, y FROM `x` LIMIT 5, 10', 'It should process limit queries with skip');
  });

  it('should process UNION queries', function () {
    let q = new SelectQuery()
      .select('x, y')
      .from('`x`')
      .union('SELECT x, y FROM `y`')
      .toQuery();

    assert.equal(q, 'SELECT x, y FROM `x` UNION ALL SELECT x, y FROM `y`', 'It should process union queries');
  });

  it('should process UNION queries with select query', function () {

    let query = new SelectQuery()
      .select('x, y')
      .from('`y`');

    let q = new SelectQuery()
      .select('x, y')
      .from('`x`')
      .union(query)
      .toQuery();

    assert.equal(q, 'SELECT x, y FROM `x` UNION ALL SELECT x, y FROM `y`', 'It should process union queries with queries');
  });

  it('should process format', function () {

    let q = new SelectQuery()
      .select('x, y')
      .from('`x`')
      .format('TabSeparated')
      .toQuery();

    assert.equal(q, 'SELECT x, y FROM `x` FORMAT TabSeparated', 'It should process limit queries with skip');

  });

});
