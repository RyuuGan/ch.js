'use strict';

let { TypeCast, intTypeList, floatTypeList } = require('../../lib/schema/cast');

describe('TypeCast', function () {

  for (let i = 0; i < intTypeList.length; i++) {
    it(`should cast ${intTypeList[i]}`, function () {
      let number = '123';

      let typeCast = new TypeCast();

      let value = typeCast.cast(intTypeList[i], number);
      assert.strictEqual(value, 123);
    });
  }

  for (let i = 0; i < floatTypeList; i++) {
    it(`should cast ${floatTypeList[i]}`, function () {
      let number = '123.73';
      let typeCast = new TypeCast();
      let value = typeCast.castMap(floatTypeList[i], number);

      assert.strictEqual(value, 123.73);
    });
  }

  it('should cast Strings', function () {
    let v = 'value';
    let typeCast = new TypeCast();

    let value = typeCast.cast('String', v);
    assert.strictEqual(value, v);
  });

  it('should cast Date', function () {
    let date = '2017-10-23';
    let typeCast = new TypeCast();

    let value = typeCast.cast('Date', date);
    assert.strictEqual(value.getTime(), new Date(date).getTime());
  });

});
